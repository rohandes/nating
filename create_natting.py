'''=============================
Author: Rohan Deshmukh
V1.1 21/4/2021
================================'''

import ipaddress
import re

#Helper function 
def new_octet(client,ccp, pos):

    diff = int(client) - int(ccp)
    if diff <0:
        new = int(client) + abs(diff)
        if new > 255:                           #IP octet value shouldn't cross 255 in ideal cases
            print("Check networks, possible error detected.")
            new = "255"
        new = f"str(int(octets[{pos}])+{abs(diff)})"
    elif diff >0:
        new = int(client) - abs(diff)
        if new <0:                              #IP octet value shouldn't go below 0 in ideal cases
            print("Check networks, possible error detected.")
            new = "0"
        new = f"str(int(octets[{pos}])-{abs(diff)})"
    else:
        new = ''

    return new

 #----------------------------------------------------------------------------------------------------------------------------

#create list of networks with client name Ex. FOLLET_NETWORK_1
def generate_block1(company_name, all_ips):
    f = open(f"{company_name}_networks_list.txt","a")
    for i,x in enumerate(all_ips):
        v = f"{company_name.upper()}_NETWORK_{i+1} = \'{x[0]}\'\n"
        f.write(v)
    f.close()
    return

#create list of IP networks Ex. ip_network_1
def generate_block2(company_name, all_ips):
    f = open(f"{company_name}_script.txt","a")
    for i in range(len(all_ips)):
        v = f"ip_network_{i+1} = ipaddress.ip_network({company_name.upper()}_NETWORK_{i+1})\n"
        f.write(v)
    f.close()
    return

#create condition block for Natting IP logic
def generate_nat_logic(company_name, all_ips):

    f = open(f"{company_name}_script.txt","a") #file written with NAT logic
    for i in range(len(all_ips)):
        v = f"ip_network_{i+1} = ipaddress.ip_network({company_name.upper()}_NETWORK_{i+1})\n"
        f.write(v)
    
    for count,networks in enumerate(all_ips):

        client_network = networks[0]
        ccp_network = networks[1]

        if re.findall('/', client_network) and re.findall('/', ccp_network):
            client_ip = client_network.split('/')[0].split('.')
            ccp_ip = ccp_network.split('/')[0].split('.')
        else:
            client_ip = client_network.split('.')
            ccp_ip = ccp_network.split('.')
            client_network = f"{client_network}/32"
            ccp_network = f"{ccp_network}/32"

        f.write(f"\n\n#{client_network} >> {ccp_network}") ## Add information of networks as comment

        ## For networks ranging from /24 to /32
        if int(client_network.split('/')[1]) >= 24:
            if count > 0:
                f.write(f"\nelif ip_address in ip_network_{count+1}:\n")
                code_block = f"\toctets = ip.split(\'.\')\n"
                if ccp_ip[0]!=client_ip[0]:
                    code_block = f"{code_block}\toctets[0] = \'{ccp_ip[0]}\'\n"
                if ccp_ip[1]!=client_ip[1]:
                    code_block = f"{code_block}\toctets[1] = \'{ccp_ip[1]}\'\n"
                if ccp_ip[2]!=client_ip[2]:
                    code_block = f"{code_block}\toctets[2] = \'{ccp_ip[2]}\'\n"
                f.write(f"{code_block}\tnatd_ip = \'.\'.join([octet for octet in octets])")
            else:
                f.write(f"\nif ip_address in ip_network_{count+1}:\n")
                code_block = f"\toctets = ip.split(\'.\')\n"
                if ccp_ip[0]!=client_ip[0]:
                    code_block = f"{code_block}\toctets[0] = \'{ccp_ip[0]}\'\n"
                if ccp_ip[1]!=client_ip[1]:
                    code_block = f"{code_block}\toctets[1] = \'{ccp_ip[1]}\'\n"
                if ccp_ip[2]!=client_ip[2]:
                    code_block = f"{code_block}\toctets[2] = \'{ccp_ip[2]}\'\n"
                f.write(f"{code_block}\tnatd_ip = \'.\'.join([octet for octet in octets])")

        ## For networks ranging from /16 to /23
        elif int(client_network.split('/')[1]) >= 16 and int(client_network.split('/')[1]) < 24:
            
            new = new_octet(client_ip[2], ccp_ip[2], pos=2)

            if count > 0:
                f.write(f"\nelif ip_address in ip_network_{count+1}:\n")
                code_block = f"\toctets = ip.split(\'.\')\n"
                if ccp_ip[0]!=client_ip[0]:
                    code_block = f"{code_block}\toctets[0] = \'{ccp_ip[0]}\'\n"
                if ccp_ip[1]!=client_ip[1]:
                    code_block = f"{code_block}\toctets[1] = \'{ccp_ip[1]}\'\n"
                if new:
                    code_block = f"{code_block}\toctets[2] = {new}\n"
                f.write(f"{code_block}\tnatd_ip = \'.\'.join([octet for octet in octets])")
            else:
                f.write(f"\nif ip_address in ip_network_{count+1}:\n")
                code_block = f"\toctets = ip.split(\'.\')\n"
                if ccp_ip[0]!=client_ip[0]:
                    code_block = f"{code_block}\toctets[0] = \'{ccp_ip[0]}\'\n"
                if ccp_ip[1]!=client_ip[1]:
                    code_block = f"{code_block}\toctets[1] = \'{ccp_ip[1]}\'\n"
                if new:
                    code_block = f"{code_block}\toctets[2] = {new}\n"
                f.write(f"{code_block}\tnatd_ip = \'.\'.join([octet for octet in octets])")

        ## For networks ranging from /8 to /15
        elif int(client_network.split('/')[1]) >= 8 and int(client_network.split('/')[1]) < 16:
            
            octet1_new = new_octet(client_ip[1], ccp_ip[1], 1)
            octet2_new = new_octet(client_ip[2], ccp_ip[2], 2)

            if count > 0:
                f.write(f"\nelif ip_address in ip_network_{count+1}:\n")
                code_block = f"\toctets = ip.split(\'.\')\n"
                if ccp_ip[0]!=client_ip[0]:
                    code_block = f"{code_block}\toctets[0] = \'{ccp_ip[0]}\'\n"
                if octet1_new:
                    code_block = f"{code_block}\toctets[1] = {octet1_new}\n"
                if octet2_new:
                    code_block = f"{code_block}\toctets[2] = {octet2_new}\n"
                f.write(f"{code_block}\tnatd_ip = \'.\'.join([octet for octet in octets])")
            else:
                f.write(f"\nif ip_address in ip_network_{count+1}:\n")
                code_block = f"\toctets = ip.split(\'.\')\n"
                if ccp_ip[0]!=client_ip[0]:
                    code_block = f"{code_block}\toctets[0] = \'{ccp_ip[0]}\'\n"
                if octet1_new:
                    code_block = f"{code_block}\toctets[1] = {octet1_new}\n"
                if octet2_new:
                    code_block = f"{code_block}\toctets[2] = {octet2_new}\n"
                f.write(f"{code_block}\tnatd_ip = \'.\'.join([octet for octet in octets])")
                

    f.write(f"\nelse:\n\tnatd_ip = ip")
    f.close()
    return

def check_duplicate_networks(all_ips):

    return

def main():
    #all_ips=[['1100.117.128.0/18','10.117.192.0/18']]
    all_ips = []
    company_name=input("Enter company name: ")
    if not company_name:
        company_name = 'test'

    nats = open("ips.txt","r").read()
    for line in nats.split('\n'):
        ips = re.split('\t| +', line)
        all_ips.append(ips)

    generate_block1(company_name, all_ips)
    #generate_block2(company_name, all_ips)
    generate_nat_logic(company_name, all_ips)
    return


if __name__ == '__main__':
    main()
